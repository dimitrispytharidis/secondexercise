package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;

public class CntTest {
    @Test
    public void testCnt() throws Exception {

        int k= new Cnt().d(6,3);
        assertEquals("Correct",2,k);

    }

    @Test
    public void testCntmax() throws Exception {

        int k= new Cnt().d(6,0);
        assertEquals("Correct",Integer.MAX_VALUE,k);

    }

}
